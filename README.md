# Tildes Stylus Template

This is a basic template with a few additions.

Add this theme to Stylus and mess with the colors. Open the front page, a thread, etc, to get a feel for the impact of different colors.

Tweaks:

 - moved feed navigation to the top and removed group name from thread pages
 - modified `Actions` menu (small, and with icons)
 - your own posts and comments will have a special indicator. Make sure you add your name near the bottom of the file (or find and replace `tomf`)
 - increased size of default comment box
 
I also included a highlight for exemplary comments, even though we can't see the actual message sent along with the tag. It's nice to see how often great comments are recognized. 

Enjoy! 

![](img/screenshot.png)